var pg =  require('./particle');

$(function(){

  var shrinkHeader = 200,
      tl = TweenLite,
      isShrunk = false;

  var shrunkVar = function () {
    isShrunk ? isShrunk = false : isShrunk = true;
  }

  var shrink = function () {
    //Pause particle effect
    pg.pause()
    //Shrink nave
    $('.nav_Wrapper').addClass('shrunk')
    $('.splash_Container img').addClass('shrunk')
    shrunkVar();
  }

  var expand = function () {
    //Start particle effect
    pg.start();
    //Expand nav
    $('.nav_Wrapper').removeClass('shrunk')
    $('.splash_Container img').removeClass('shrunk')
    shrunkVar();
  }

  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
          if (!isShrunk) {
            shrink();
          };
        }
        else {
          if (isShrunk) {
            expand();
          };
        }
  });

  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }
});