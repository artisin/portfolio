console.log('App loaded');


//App Intro
$(document).ready(function() {
  var tl = new TimelineLite();
  //Intro 
  tl.to('.mainLogo', 1, {opacity:1})
    .to('.introOne', 1, {css:{opacity: 1, transform:"scale(1)"}})
    .to('.introOne', 0.35, {css:{autoAlpha:0, transform:"scale(0)"}})
    .to('.introTwo', 1, {css:{opacity: 1, transform:"scale(1)"}})
    .to('.introTwo', 0.35, {css:{autoAlpha:0, transform:"scale(0)"}})
    .to('.introThree', 1, {css:{opacity: 1, transform:"scale(1)"}})
    .to('.introThree', 0.35, {css:{autoAlpha:0, transform:"scale(0)"}})
    .to('.introFour', 1, {css:{opacity: 1, transform:"scale(1)"}})
    .to('.introFour', 0.35, {css:{autoAlpha:0, transform:"scale(0)"}})
  
});



//Header
require('./header');

//Particle background
require('./particle');


//tabs
require('./tabs');